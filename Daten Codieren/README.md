# Daten Codieren Aufgaben

## A. Aufgaben zu numerischen Codes

1. <br> ![](/Daten%20Codieren/images/1.png)
2. 3. 4. 
| DEC    | HEX   | BIN                 | 
| ------ | ------|---------------------|
| 911    | -     |      0101 0100 1010 |
| 178    | -     |           1011 0110 |
| 37557  | E2A5  | 1110 0010 1010 0101 |

5. <br> 1101 1001 + 0111 0101 = 0001 0100 1110

6. <br> a. 1100'0000.1010'1000.0100'1100.1101'0011 = 192.168.76.211 (IPv4) <br> b. 1011’1110-1000’0011-1000’0101-1101’0101-1110’0100-1111’1110 = BE:83:85:D5:E4:FE (MAC-address)

7. `chmod 751 CreateWeeklyReport` <br>
chmod (berechtigungen bearbeiten) <br>
**751**: <br> **7** (4 + 2 + 1) -> Owner, <br> **5** (4 + 0 + 1) -> Groups, <br> **1** (0 + 0 + 1) -> Others <br>
Jede dieser Zahlen ist eine Summe der Rechte, die wie folgt kodiert sind: 
<br> 4 steht für "lesen" (read) <br>2 steht für "schreiben" (write) <br>1 steht für "ausführen" (execute) <br>

8. 1000 0000 ? 2^7
9. *Optional*
10. *Optional*
11. 
- Unsigned Integer <br>
 kleinster Binärwert: 0000 0000 -> 0 <br>
 grösster Binärwert: 1111 1111 -> 255

 - Signed Integer <br>
 kleinster Binärwert: 1000 0000 -> -128 <br>
 grösster Binärwert: 0111 1111 -> 127

 - +83 -> 0101 0011

 - -83 -> 1010 1101

 - 0

 - 0000 0000

 - Bei einem Byte (8 Bits) liegt der Bereich für vorzeichenbehaftete Ganzzahlen zwischen -128 und +127 (Zweierkomplement). +150 liegt außerhalb dieses Bereichs und kann daher nicht mit einem Byte dargestellt werden. 

12. 


